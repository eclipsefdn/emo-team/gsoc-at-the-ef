## **Project Idea Name**
<!-- 
If you are interested in mentoring a project please use this template to submit your Project Idea for the GSoC programme.
The format suggested below is exactly that, a suggestion. Play with the layout, but please make sure to provide all relevant information. Please be sure to follow branding guidelines (e.g. "Eclipse Dash", not "Dash"). Links to content hosted elsewhere are fine, but bear in mind that this GitLab project and its issues will be the main point of contact for people who are not already inside your community.
-->

### **Description** 
Of the Example Project idea with links to more information, bugs, and other useful content. This could include a list of specific objectives

### **Links to Eclipse Project** 
Both to the [EF PMI](https://projects.eclipse.org/) and relevant repositories/websites.

### **Expected outcomes** 
List of the results expected to be achieved after completing of the project.

### **Skills required/preferred** 
List of skill set required to be able to complete the project in the proposed time.

### **Project size** 
90/175/350 hours

### **Possible mentors:** 
[Somebody Mentor](mailto:somebody@someplace.com)

### **Rating** 
Easy, medium or hard rating of each project.

/label ~"GSoC 2025" ~"Project Idea"
