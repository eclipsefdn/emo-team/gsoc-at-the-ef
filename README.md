# GSoC at the EF

_**The Eclipse Foundation is getting ready to submit its application to GSoC 2025. Thank you for your interest in open source software development at the     Eclipse Foundation. The Eclipse community is a great place to spend a summer learning, coding, participating and contributing. We are an exciting open source project with a vibrant community, and we look forward to your application and your project ideas.**_

Google Summer of[ ](https://summerofcode.withgoogle.com/)Code (GSoC) is a global, online programme focused on bringing new contributors into open source software development. GSoC Contributors work with an open source organization on a 12+ week programming project under the guidance of mentors. GSoC Contributors who pass their evaluations and comply with the Programme rules will receive a stipend from Google for their participation. If you want to learn more on how the programme works, please visit the [GSoC website](https://summerofcode.withgoogle.com/).

The programme is open to students and to beginners in open source software development.

If you want to become a mentor take a few minutes to add your project ideas to the 2024 ["Ideas" page](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/issues/?sort=due_date&state=opened&label_name%5B%5D=Project%20Idea&label_name%5B%5D=GSoC%202024&first_page_size=20). If you're truly interested in participating please take the time to create a complete project idea as defined by the criteria available [**here**](https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list). We are continuously improving the landing page with some more information for students. Please feel free to add or otherwise improve the information there. Once we submit our application we’ll let you know if we get accepted how to become an official mentor and when we'll be ready to accept you.

A good way to meet those involved with the program is to participate in the [soc-dev mailing list](https://accounts.eclipse.org/mailing-list/soc-dev).

The program is administered by the [Eclipse Management Organization (EMO)](emo@eclipse.org). It's best if you use the public communication channel whenever possible; however, if you need to communicate in private, please feel free to send a note (please use the public channels for any project-related discussion).

In this page your find information regarding:
- [Students](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/blob/main/README.md#students)
- [Mentors](https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/wikis/home/Google-Summer-of-Code#mentors)
- [Submitting a Project Idea](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/blob/main/README.md#submitting-a-project-idea)
- [Project Ideas Catalogue (current and past years)](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/blob/main/README.md#project-ideas-catalogue)

## Students

Search through our [Project Ideas](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/issues/?sort=due_date&state=opened&label_name%5B%5D=Project%20Idea&label_name%5B%5D=GSoC%202024&first_page_size=20) to find something interesting. You can browse all of our Open Source Projects directly [here](https://www.eclipse.org/projects/). Note that you can find links to the Git repositories by clicking the "Developer Resources" tab on a page. All repositories should include a CONTRIBUTING file with project-specific contribution guidance.

Sign up for the [soc-dev list](https://dev.eclipse.org/mailman/listinfo/soc-dev) to connect with our mentors and other members of the Eclipse GSoC community.

Start by [creating an Eclipse Foundation account](https://accounts.eclipse.org/user/register?destination=user) and sign the [Eclipse Contributor Agreement](https://www.eclipse.org/legal/ECA.php) to ensure that you are ready to make contributions.

Talk with your potential mentors about their expectations of GSoC applicants. When writing your application make sure to get your mentor involved.

#### Some Useful Stuff

There's help regarding how we expect Git Commits to be structured in the [Eclipse Project Handbook](https://www.eclipse.org/projects/handbook/#resources-commit) (please don't feel like you have to read the entire handbook; treat it like a resource).

The Eclipse Foundation values rigorous Intellectual Property management. We care deeply about honoring software licenses and copyright. By signing the [ECA](https://www.eclipse.org/legal/ECA.php), you attest that all contributions that you make are authored 100% by you and that you have the necessary rights to contribute to our open source projects. Please work with a committer if you need to include third-party content with your contributions (we do have a due diligence process for handing third-party content).

## Mentors

After we've been accepted as a mentoring organization, we will have the opportunity to add mentors to the Eclipse Foundation's Google Summer of Code team. Google's process requires that mentors be invited to join the mentoring organization's team. The EMO will send a note to soc-dev@eclipse.org when we're ready to start inviting mentors (that is, we'll tell you when it's time to ask to be invited).

Anybody who is a committer on an open source project hosted by the Eclipse Foundation can be a mentor. When the time comes, we'll ask you to send emo@eclipse.org a note with your name, the email address for your Google account, and the names of the projects for which you have committer status, and we'll set up an invitation. Non committers may be mentors, only if they have a project lead who can vouch for them. Project leads should expect to be asked why the individual is not a committer.

Mentoring takes a couple of different forms. First, as a mentor, you will be invited to help the team review and select student proposals. Student proposals will be accessible via the interface provided by Google. While reviewing proposals, mentors may opt select a proposal and offer to mentor a student. At that point, a mentor commits to working with the student for the term if that student proposal is ultimately selected.

Mentors should familiarize themselves with the [Google Summer of Code Mentoring Guide.](https://google.github.io/gsocguides/mentor/)

In the lead up to the start of the programme in any given year, we need to assemble an "ideas" page for students. All Eclipse Committers can add project ideas before being formally invited as a mentor. Add your ideas to the current year "ideas" page (see below).

Sign up for the [soc-dev list](https://dev.eclipse.org/mailman/listinfo/soc-dev) to connect with students and other members of the Eclipse GSoC community.

Please keep in mind that--for this to work--you will be required to invest some time looking at student proposals for projects that you might not necessarily have a direct interested in. We still value your input on these proposals.

## Submitting a Project Idea

If you are interested in mentoring a project please [submit your project idea by opening an issue in the GSoC for EF repository](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/issues/new?issuable_template=GSoC_projectidea).
You'll find a template for submitting your proposal when opening a new issue. Please add your content there. The format suggested below is just an 
idea. Play with the layout, but please make sure to provide all relevant information. Please be sure to follow branding guidelines (e.g. "Eclipse Dash", not "Dash"). Links to content hosted elsewhere are fine, but bear in mind that this GitLab project and its issues will be the main point of contact for people who are not already inside your community.

Starting 2023, projects can take \~90, \~175 hours **or** \~350 hours for GSoC contributors to complete.

Each project on the Ideas list should include:

- a project title/description
- more detailed description of the project (3-5 sentences)
- expected outcomes
- skills required/preferred
- possible mentors
- expected size of project (90, 175 or 350 hour ).
- an easy, medium or hard rating of each project.

### Example Project idea

**Description** of the Example Project idea with links to more information, bugs, and other useful content. This could include a list of specific objectives

**Links to Eclipse Project** in the [EF PMI](https://projects.eclipse.org/) and repositories

**Expected outcomes** list of the results expected to be achieved after completing of the project.

**Skills required/preferred** list of skill set required to be able to complete the project in the proposed time.

**Project size** 90/175/350 hours

**Possible mentors:** [Somebody Mentor](mailto:somebody@somplace.com)

**Rating** Easy, medium or hard rating of each project.

## Project Ideas Catalogue

**Current Year**

Starting 2024, the EF will use this [GitLab project repository](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/issues) to collect project ideas. In the following link you'll find a list of the proposed Project Ideas, already filtered for the GSoC 2025 programme. Please feel free to make your questions and provide your feedback directly in the issues.

* [Google Summer of Code 2025](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/issues/?sort=due_date&state=opened&label_name%5B%5D=Project%20Idea&label_name%5B%5D=GSoC%202025&first_page_size=20)

**Past Years**
* [Google Summer of Code 2024](https://gitlab.eclipse.org/eclipsefdn/emo-team/gsoc-at-the-ef/-/issues/?sort=due_date&state=closed&label_name%5B%5D=Project%20Idea&label_name%5B%5D=GSoC%202024&first_page_size=50)
* [Google Summer of Code 2023](https://wiki.eclipse.org/Google_Summer_of_Code_2023_Ideas)
* [Google Summer of Code 2022](https://wiki.eclipse.org/Google_Summer_of_Code_2022_Ideas)
* [Google Summer of Code 2020](https://wiki.eclipse.org/Google_Summer_of_Code_2020_Ideas)
* [Google Summer of Code 2019](https://wiki.eclipse.org/Google_Summer_of_Code_2019_Ideas)
* [Google Summer of Code 2018](https://wiki.eclipse.org/Google_Summer_of_Code_2018_Ideas)
* [Google Summer of Code 2017](https://wiki.eclipse.org/Google_Summer_of_Code_2017_Ideas)
* [Google Summer of Code 2016](https://wiki.eclipse.org/Google_Summer_of_Code_2016_Ideas)
* [Google Summer of Code 2015](https://wiki.eclipse.org/Google_Summer_of_Code_2015_Ideas)
* [Google Summer of Code 2014](https://wiki.eclipse.org/Google_Summer_of_Code_2014_Ideas)
* [Google Summer of Code 2013](https://wiki.eclipse.org/Google_Summer_of_Code_2013_Ideas)
* [Google Summer of Code 2012](https://wiki.eclipse.org/Google_Summer_of_Code_2012_Ideas)
* [Google Summer of Code 2011](https://wiki.eclipse.org/Google_Summer_of_Code_2011_Ideas)
* [Google Summer of Code 2010](https://wiki.eclipse.org/Google_Summer_of_Code_2010_Ideas)
* [Google Summer of Code 2009](https://wiki.eclipse.org/Google_Summer_of_Code_2009_Ideas)
* [Google Summer of Code 2008](https://wiki.eclipse.org/Google_Summer_of_Code_2008_Ideas)
* [Google Summer of Code 2007](https://wiki.eclipse.org/Google_Summer_of_Code_2007_Ideas)
* [Google Summer of Code 2006](https://wiki.eclipse.org/Google_Summer_of_Code_2006_Ideas)
